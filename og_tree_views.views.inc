<?php
/**
 * @file
 * Define all OG Tree views
 */

/**
* Implementation of hook_views_data() to register all of the basic handlers
* views uses.
*/
function og_tree_views_views_data() {
  $data['field_data_group_audience']['table']['group']  = t('OG Tree Views');

  $data['field_data_group_audience']['entity_id'] = array(
    'title' => t('Entity id'),
    'help' => t('Use the etid of a group content as a starting point to retrieve other groups in the hierarchy.'),
    'argument' => array(
      'handler' => 'og_tree_views_handler_argument_etid',
    ),
    'sort' => array(
      'title' => t('Hierarchy order'),
      'help' => t('Sort the group'),
      'handler' => 'og_tree_views_handler_sort_hierarchy',
    ),

  );
  
  return $data;
}


/**
 * Implementation of hook_views_handlers().
 */
function og_tree_views_views_handlers() {
  return array(
    'info' => array(
        'path' => drupal_get_path('module', 'og_tree_views') . '/includes',
    ),
    'handlers' => array(
      // arguments
      'og_tree_views_handler_argument_etid' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
      // sorts
      'og_tree_views_handler_sort_hierarchy' => array(
        'parent' => 'views_handler_sort',
      ),
    ),
  );
}

