<?php
/**
 * @file
 * Sort by OG group audiences
 */

/**
 * This handler will sort all content groups by their parents
 * to retrieve the hierarchy
 *
 * @ingroup views_sort_handlers
 */
class og_tree_views_handler_sort_hierarchy extends views_handler_sort {
  /**
   * The sort cannot be exposed
   */
  function can_expose() { return FALSE; }

  function query() {
    $groups = og_get_tree();

    if (!empty($groups)) {
      foreach ($groups as $group) $keys[] = $group->etid;      
      $sort_order = implode(',', $keys);
      $base_table = $this->query->base_table;
      $base_field = $this->query->base_field;
      $formula = "FIELD($base_table.$base_field, $sort_order)";

      $this->query->add_orderby(NULL, $formula, $this->options['order'], '_' . $this->field);
    }
  }
}